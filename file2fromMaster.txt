This is the last common commit between master and develop.

If you need to rewind rebase or merge operation done on branch develop :
-checkout develop
-git reset --hard 5249f9f4dcf95967fc60406f26d40cdaf2e4af8e (6 first characters of hash also works)
-create file4FromDevelop.txt
-commit

If you need to rewind master :
-checkout master
-git reset --hard 5249f9f4dcf95967fc60406f26d40cdaf2e4af8e
-create file3FromMaster.txt
-commit

Same goes for master
